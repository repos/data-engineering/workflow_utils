import os
import pytest
from unittest import mock
from workflow_utils.util import (
    safe_filename,
    find_java_home,
    instantiate,
    package_version
)


@pytest.mark.parametrize('name, expected', [
    ("a/b/c", "a_b_c"),
    ("d.e-f", "d.e-f"),
    ("org.apache.hadoop:hadoop-yarn-client:2.10.1", "org.apache.hadoop_hadoop-yarn-client_2.10.1"),
])
def test_safe_filename(name, expected):
    assert safe_filename(name) == expected


class MyClass():  # pylint: disable=too-few-public-methods
    def __init__(self, name):
        self.name = name

def test_instantiate():
    instance = instantiate(
        class_name='tests.test_util.MyClass',
        name='a1'
    )

    assert isinstance(instance, MyClass), \
        'instantiate should work with class_name string as kwarg'
    assert instance.name == 'a1'

    instance = instantiate(**{
        'class_name': 'tests.test_util.MyClass',
        'name': 'a2'
    })
    assert isinstance(instance, MyClass), \
        'instantiate should work with class_name string in kwargs dict'
    assert instance.name == 'a2'

    with pytest.raises(ValueError):
        instantiate(
            **{'class_name': 'NotDefinedClass', 'name': 'a3'}
        )

    with pytest.raises(AttributeError):
        instantiate(
            **{'class_name': 'tests.test_util.NotDefinedClass', 'name': 'a3'}
        )

def test_instantiate_fn_ref():
    test_base_uri = '/wmf/cache/artifacts/airflow/analytics/mvn'

    instance = instantiate(
        class_name='workflow_utils.artifact.FsArtifactCache',
        base_uri=test_base_uri,
        cache_key_fn='workflow_utils.artifact.cache.cache_key_maven',
    )
    from workflow_utils.artifact import FsArtifactCache
    from workflow_utils.artifact.cache import cache_key_maven

    assert isinstance(instance, FsArtifactCache), \
        'instantiate should work with class_name string as kwarg'

    assert instance.base_uri == test_base_uri, \
        'instantiate did not load the correct class constructor argument'

    assert instance.cache_key_fn is cache_key_maven, \
        'instantiate did not properly load a referenced function'



def test_find_java_home():
    with mock.patch.dict(os.environ, {'JAVA_HOME': '/nonexistent'}):
        assert find_java_home() is None

    with mock.patch.dict(os.environ, {'JAVA_HOME': '/'}):
        assert find_java_home() == '/'
