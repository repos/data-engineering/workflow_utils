from copy import deepcopy
import io
import filecmp
import os
import pytest
import re
from datetime import datetime

from workflow_utils.artifact import (
    Artifact,
    FsArtifactSource,
    MavenArtifactSource,
    FsArtifactCache,
    FsMavenArtifactCache,
)

from workflow_utils.util import safe_filename

FAKE_JAR_FILE_CONTENT = "FAKE JAR"


@pytest.fixture(name='local_maven_repo')
def fixture_local_maven_repo(tmpdir):
    """
    Yields a path to a tmpdir with a maven artifact jar in it
    """
    maven_repo_path = os.path.join(tmpdir, 'local_maven_repo')
    a1_dir = os.path.join(maven_repo_path, 'org/apache/hadoop/hadoop-yarn-client/2.10.1')
    a1_file = os.path.join(a1_dir, 'hadoop-yarn-client-2.10.1.jar')
    os.makedirs(a1_dir)
    with open(a1_file, 'wt', encoding="utf-8") as file:
        file.write(FAKE_JAR_FILE_CONTENT)

    yield maven_repo_path


@pytest.fixture(name='local_directory')
def fixture_local_directory(tmpdir):
    """
    Yields a path to a tmpdir with various files in it
    """
    git_repo_path = os.path.join(tmpdir, 'fake_git_repo')

    d1_dir = os.path.join(git_repo_path, 'git_repo_folder', 'dir1')
    d1_file = os.path.join(d1_dir, 'd1.txt')
    os.makedirs(d1_dir)
    with open(d1_file, 'wt', encoding="utf-8") as file:
        file.write("FAKE TXT FILE 1")

    d2_dir = os.path.join(git_repo_path, 'git_repo_folder', 'dir2')
    d2_file = os.path.join(d2_dir, 'd2.txt')
    os.makedirs(d2_dir)
    with open(d2_file, 'wt', encoding="utf-8") as file:
        file.write("FAKE TXT FILE 2")

    yield git_repo_path


@pytest.fixture(name='fs_artifact_source_local')
def fixture_fs_artifact_source_local(local_directory):
    """
    Yields a path to a tmpdir with various files in it and
    an instantiated FsArtifactSource
    """
    yield FsArtifactSource(local_directory)


@pytest.fixture(name='fs_artifact_source')
def fixture_fs_artifact_source(local_maven_repo):
    """
    Yields a path to a tmpdir with a maven artifact jar in it and
    an instantiated FsArtifactSource
    """
    yield FsArtifactSource(local_maven_repo)


@pytest.fixture(name='maven_artifact_source')
def fixture_maven_artifact_source(local_maven_repo):
    """
    Yields an instantiated MavenArtifactSource using
    a local maven tmpdir repo with an artifact in it.
    """
    yield MavenArtifactSource(local_maven_repo)


@pytest.fixture(name='fs_artifact_cache')
def fixture_fs_artifact_cache(tmpdir):
    """
    Yields a FsArtifactCache with one item in the cache.
    """

    local_cache_dir = os.path.join(tmpdir, 'artifact_cache_dir')
    a1_file = os.path.join(
        local_cache_dir,
        'org_apache_hadoop_hadoop-yarn-client_2.10.1_hadoop-yarn-client-2.10.1.jar'
    )
    os.makedirs(local_cache_dir)
    with open(a1_file, 'wt', encoding="utf-8") as file:
        file.write(FAKE_JAR_FILE_CONTENT)

    yield FsArtifactCache(local_cache_dir)


def cache_key_current(artifact: Artifact) -> str:
    """Appends a folder named "current" after a safe-encoded
    Artifact name.

    :param artifact: Artifact
    """
    return os.path.join(safe_filename(artifact.name), "current")


@pytest.fixture(name='custom_fs_artifact_cache_current')
def fixture_custom_fs_artifact_cache_current(tmpdir):
    """
    Yields a FsVersionedArtifactCache with no items in the cache,
    with "current" as part of destination path.
    """

    local_cache_dir = os.path.join(tmpdir, 'versioned_artifact_cache_dir')

    yield FsArtifactCache(local_cache_dir, cache_key_current)


def cache_key_tstamped(artifact: Artifact) -> str:
    """Appends a folder named after current timestamp after a safe-encoded
    Artifact name.

    :param artifact: Artifact
    """

    tstamp = datetime.utcnow().strftime('%Y%m%d%H%M%S')

    return os.path.join(safe_filename(artifact.name), tstamp)


@pytest.fixture(name='custom_fs_artifact_cache_tstamped')
def fixture_custom_fs_artifact_cache_tstamped(tmpdir):
    """
    Yields a FsVersionedArtifactCache with no items in the cache,
    with a tstamp as part of destination path.
    """

    local_cache_dir = os.path.join(tmpdir, 'versioned_artifact_cache_dir')

    yield FsArtifactCache(local_cache_dir, cache_key_tstamped)


@pytest.fixture(name='fs_maven_artifact_cache')
def fixture_fs_maven_artifact_cache(tmpdir):
    """
    Yields a FsMavenArtifactCache with one item in the cache.
    """

    local_cache_dir = os.path.join(tmpdir, 'maven_artifact_cache_dir')
    a1_file = os.path.join(
        local_cache_dir,
        'org_apache_hadoop_hadoop-yarn-client_2.10.1_hadoop-yarn-client-2.10.1.jar'
    )
    os.makedirs(local_cache_dir)
    with open(a1_file, 'wt', encoding="utf-8") as file:
        file.write(FAKE_JAR_FILE_CONTENT)

    yield FsMavenArtifactCache(local_cache_dir)


@pytest.fixture(name='artifact')
def fixture_artifact(maven_artifact_source, tmpdir):
    local_cache_dir = os.path.join(tmpdir, 'artifact_cache_dir_0')
    yield Artifact(
        id='org.apache.hadoop:hadoop-yarn-client:2.10.1',
        source=maven_artifact_source,
        # Use a new empty cache
        caches=[FsMavenArtifactCache(local_cache_dir)],
        name='hadoop-yarn-client-2.10.1.jar'
    )


@pytest.fixture(name='artifact_configs')
def fixture_artifact_configs(
    fs_artifact_source,
    maven_artifact_source,
    fs_artifact_cache,
    fs_maven_artifact_cache
):
    yield {
        'artifacts': {
            'hadoop-yarn-client-2.10.1.jar': {
                'id': 'org.apache.hadoop:hadoop-yarn-client:2.10.1',
                'source': 'maven_source',
                'caches': ['maven_cache'],
            },
            'my_artifact.zip': {
                'id': 'my_artifact.zip',
            },
        },
        'artifact_sources': {
            'fs_source': {
                'class_name': 'workflow_utils.artifact.FsArtifactSource',
                'base_uri': fs_artifact_source.base_uri
            },
            'maven_source': {
                'class_name': 'workflow_utils.artifact.MavenArtifactSource',
                'base_uri': maven_artifact_source.base_uri
            },
        },
        'artifact_caches': {
            'fs_cache': {
                'class_name': 'workflow_utils.artifact.FsArtifactCache',
                'base_uri': fs_artifact_cache.base_uri
            },
            'maven_cache': {
                'class_name': 'workflow_utils.artifact.FsMavenArtifactCache',
                'base_uri': fs_maven_artifact_cache.base_uri
            },
        },
        'default_artifact_source': 'fs_source',
        'default_artifact_caches': ['fs_cache']
    }


# == Test ArtifactSources ==
def test_fs_artifact_source_url(fs_artifact_source):
    """
    FsArtifactSource.url() should return the artifact id
    prefixed with the base_uri.
    """
    artifact_id = 'org/apache/hadoop/hadoop-yarn-client/2.10.1/hadoop-yarn-client-2.10.1.jar'
    artifact = Artifact(id=artifact_id, source=fs_artifact_source)
    expected = os.path.join(fs_artifact_source.base_uri, artifact_id)
    assert fs_artifact_source.url(artifact) == expected

def test_fs_artifact_source_exists(fs_artifact_source):
    artifact_id = 'org/apache/hadoop/hadoop-yarn-client/2.10.1/hadoop-yarn-client-2.10.1.jar'
    artifact = Artifact(id=artifact_id, source=fs_artifact_source)
    assert fs_artifact_source.exists(artifact)
    artifact_not_exists = Artifact(id='not/exists', source=fs_artifact_source)
    assert not fs_artifact_source.exists(artifact_not_exists)

def test_fs_artifact_source_size(fs_artifact_source):
    artifact_id = 'org/apache/hadoop/hadoop-yarn-client/2.10.1/hadoop-yarn-client-2.10.1.jar'
    artifact = Artifact(id=artifact_id, source=fs_artifact_source)
    size = fs_artifact_source.size(artifact)
    assert size is not None
    assert size > 0

def test_fs_artifact_source_size_no_file(fs_artifact_source):
    artifact_id = 'non_existent'
    artifact = Artifact(id=artifact_id, source=fs_artifact_source)
    size = fs_artifact_source.size(artifact)
    assert size is None

def test_fs_artifact_source_size_remote_directory(monkeypatch, fs_artifact_source):
    artifact_id = 'non_existent'
    artifact = Artifact(id=artifact_id, source=fs_artifact_source)

    def mocksize(*args):
        return None

    from fsspec.spec import AbstractFileSystem
    monkeypatch.setattr(AbstractFileSystem, "size", mocksize)

    size = fs_artifact_source.size(artifact)
    assert size is None

def test_fs_artifact_source_fs_base_uri(fs_artifact_source):
    artifact_id = 'org/apache/hadoop/hadoop-yarn-client/2.10.1/hadoop-yarn-client-2.10.1.jar'
    artifact = Artifact(id=artifact_id, source=fs_artifact_source)
    assert fs_artifact_source.fs(artifact).protocol == 'file'

def test_fs_artifact_source_fs_no_base_uri():
    fs_artifact_source = FsArtifactSource()

    assert fs_artifact_source.fs(
        Artifact(id="/tmp/file", source=fs_artifact_source)
    ).protocol == 'file'

    assert fs_artifact_source.fs(
        Artifact(id="file:///tmp/file", source=fs_artifact_source)
    ).protocol == 'file'

    assert fs_artifact_source.fs(
        Artifact(id="rel/path/file", source=fs_artifact_source)
    ).protocol == 'file'

    assert fs_artifact_source.fs(
        Artifact(id="https://example.org/path/to/file.tgz", source=fs_artifact_source)
    ).protocol == 'https'

def test_maven_artifact_source_url(maven_artifact_source):
    """
    MavenArtifactSource.url() should translate from a maven coordinate to a URL.
    """
    artifact_id = 'org.apache.hadoop:hadoop-yarn-client:2.10.1'
    artifact = Artifact(id=artifact_id, source=maven_artifact_source)
    expected = os.path.join(
        maven_artifact_source.base_uri,
        'org/apache/hadoop/hadoop-yarn-client/2.10.1/hadoop-yarn-client-2.10.1.jar'
    )
    assert maven_artifact_source.url(artifact) == expected


# == Test ArtifactCaches ==

def test_fs_artifact_cache_url(fs_artifact_source, fs_artifact_cache):
    """
    FsArtifactCache.url() should just return the
    a safe filename of id prefixed with the cache base uri.
    """
    artifact_id = 'org/apache/hadoop/hadoop-yarn-client/2.10.1/hadoop-yarn-client-2.10.1.jar'
    artifact = Artifact(id=artifact_id, source=fs_artifact_source, caches=[fs_artifact_cache])
    expected = os.path.join(
        fs_artifact_cache.base_uri,
        'org_apache_hadoop_hadoop-yarn-client_2.10.1_hadoop-yarn-client-2.10.1.jar'
    )
    assert fs_artifact_cache.url(artifact) == expected


def test_fs_artifact_cache_exists(fs_artifact_source, fs_artifact_cache):
    artifact_id = 'org/apache/hadoop/hadoop-yarn-client/2.10.1/hadoop-yarn-client-2.10.1.jar'
    artifact = Artifact(id=artifact_id, source=fs_artifact_source, caches=[fs_artifact_cache])
    assert fs_artifact_cache.exists(artifact)
    artifact_not_exists = Artifact(id='not/exists', source=fs_artifact_source, caches=[fs_artifact_cache])
    assert not fs_artifact_cache.exists(artifact_not_exists)


def test_fs_artifact_cache_size(fs_artifact_source,fs_artifact_cache):
    artifact_id = 'org/apache/hadoop/hadoop-yarn-client/2.10.1/hadoop-yarn-client-2.10.1.jar'
    artifact = Artifact(id=artifact_id, source=fs_artifact_source, caches=[fs_artifact_cache])
    size = fs_artifact_cache.size(artifact)
    assert size is not None
    assert size > 0


def test_fs_artifact_cache_size_no_file(fs_artifact_source, fs_artifact_cache):
    artifact_id = 'non_existent'
    artifact = Artifact(id=artifact_id, source=fs_artifact_source, caches=[fs_artifact_cache])
    size = fs_artifact_cache.size(artifact)
    assert size is None


def test_fs_artifact_cache_size_remote_directory(monkeypatch, fs_artifact_source_local, fs_artifact_cache):
    """Re-create the conditions of querying the size of a remote
    directory, implementation of which returns `None` in such case.
    """
    artifact_id = 'non_existent'
    artifact = Artifact(id=artifact_id, source=fs_artifact_source_local, caches=[fs_artifact_cache])

    def mocksize(*args):
        return None

    from fsspec.spec import AbstractFileSystem
    monkeypatch.setattr(AbstractFileSystem, "size", mocksize)

    size = fs_artifact_cache.size(artifact)
    assert size is None


def test_fs_artifact_cache_put(fs_artifact_source, fs_artifact_cache):
    """
    Test that a put of new content will result in the content
    being written into the cache at the cache url for the artifact id.
    """
    artifact_id = 'org/apache/hadoop/hadoop-yarn-client/2.10.1/hadoop-yarn-client-2.10.1.jar'
    artifact = Artifact(id=artifact_id, source=fs_artifact_source, caches=[fs_artifact_cache])
    # Test source artifact input
    new_fake_file_content = "NEW FAKE JAR CONTENT"
    with open(fs_artifact_source.url(artifact), 'wt', encoding="utf-8") as file:
        file.write(new_fake_file_content)

    fs_artifact_cache.put(artifact)

    with open(fs_artifact_cache.url(artifact), mode='r') as file:
        assert file.read() == new_fake_file_content


def test_custom_fs_artifact_cache_put(
        fs_artifact_source_local,
        custom_fs_artifact_cache_current,
        custom_fs_artifact_cache_tstamped
):
    """
    Test that a put of new content will result in the content
    being written into the cache at the cache url for the artifact id.
    """
    artifact_id = 'git_repo_folder'
    artifact = Artifact(
        id=artifact_id,
        source=fs_artifact_source_local,
        caches=[
            custom_fs_artifact_cache_current,
            custom_fs_artifact_cache_tstamped,
        ],
    )

    custom_fs_artifact_cache_current.put(artifact)
    custom_fs_artifact_cache_tstamped.put(artifact)

    cache_url = custom_fs_artifact_cache_current.url(artifact)

    assert cache_url.endswith(f"versioned_artifact_cache_dir/{artifact_id}/current")
    assert os.path.exists(cache_url)

    cache_dir_url = os.path.dirname(cache_url)
    source_dir_url = fs_artifact_source_local.url(artifact)

    # test that we have the tstamped and "current" subfolders
    subfolders = os.listdir(cache_dir_url)

    assert "current" in subfolders

    subfolders.remove("current")

    assert len(subfolders) == 1

    tstamped_dir = subfolders[0]

    assert re.match(r"\d{14}", tstamped_dir) is not None

    for folder_name in ["current", tstamped_dir]:
        for root, dirs, files in os.walk(source_dir_url):
            for fname in files:
                relative_dir_path = os.path.relpath(root, source_dir_url)
                file_in_cache = os.path.join(cache_dir_url, folder_name, relative_dir_path, fname)
                file_in_source = os.path.join(root, fname)

                assert filecmp.cmp(file_in_cache, file_in_source, shallow=False) is True, f"Test file {fname} differs between source and cache"


def test_custom_fs_artifact_cache_delete(
        fs_artifact_source_local,
        custom_fs_artifact_cache_current
):
    """
    Test that a put of new content will result in the content
    being written into the cache at the cache url for the artifact id.
    """
    artifact_id = 'git_repo_folder'
    artifact = Artifact(
        id=artifact_id,
        source=fs_artifact_source_local,
        caches=[
            custom_fs_artifact_cache_current,
        ],
    )

    custom_fs_artifact_cache_current.put(artifact)

    cache_url = custom_fs_artifact_cache_current.url(artifact)

    assert cache_url.endswith(f"versioned_artifact_cache_dir/{artifact_id}/current")

    assert os.path.exists(cache_url)
    assert custom_fs_artifact_cache_current.exists(artifact)

    custom_fs_artifact_cache_current.delete(artifact)

    assert not os.path.exists(cache_url)
    assert not custom_fs_artifact_cache_current.exists(artifact)


def test_fs_artifact_cache_delete(fs_artifact_source, fs_artifact_cache):
    artifact_id = 'org/apache/hadoop/hadoop-yarn-client/2.10.1/hadoop-yarn-client-2.10.1.jar'
    artifact = Artifact(id=artifact_id, source=fs_artifact_source, caches=[fs_artifact_cache])
    fs_artifact_cache.delete(artifact)
    assert not fs_artifact_cache.exists(artifact)


def test_fs_maven_artifact_cache_url(maven_artifact_source, fs_maven_artifact_cache):
    """
    FsMavenArtifactCache.url() should just return the
    path in a maven repo for a maven coordinate artifact id.
    """
    artifact_id = 'org.apache.hadoop:hadoop-yarn-client:2.10.1'
    artifact = Artifact(id=artifact_id, source=maven_artifact_source, caches=[fs_maven_artifact_cache])
    expected = os.path.join(
        fs_maven_artifact_cache.base_uri,
        'org/apache/hadoop/hadoop-yarn-client/2.10.1/hadoop-yarn-client-2.10.1.jar'
    )
    assert fs_maven_artifact_cache.url(artifact) == expected


# == Test Artifact ==
def test_artifact_locators(artifact):
    assert len(artifact.locators(must_exist=False)) == 2, \
        "artifact has a source and one cache, so # of locators should be 2"
    assert len(artifact.locators(must_exist=True)) == 1, \
        "artifact has not yet been cached, so # of locatators with must_exist should be 1"


def test_artifact_exists(artifact):
    assert artifact.exists() is True


def test_artifact_exists_in_cache(artifact):
    assert artifact.exists_in_cache() is False


def test_artifact_urls(artifact):
    assert len(artifact.urls(must_exist=False)) == 2, \
        "artifact has a source and one cache, so # of urls should be 2"
    assert len(artifact.urls(must_exist=True)) == 1, \
        "artifact has not yet been cached, so # of urls with must_exist should be 1"


def test_artifact_cached_urls(artifact):
    assert len(artifact.cached_urls(must_exist=False)) == 1, \
        "artifact has not yet been cached, # urls that might exist in caches should be 1"
    assert len(artifact.cached_urls(must_exist=True)) == 0, \
        "artifact has not yet been cached, # of urls that must exist in caches should be 0"


def test_artifact_cached_url(artifact):
    assert artifact.cached_url(must_exist=False) is not None, \
        "artifact has not yet been cached, should be a cached url that might exist"
    assert artifact.cached_url(must_exist=True) is None, \
        "artifact has not yet been cached, so cached url should be None"


def test_artifact_cache_put_and_delete(artifact):
    artifact.cache_put()
    assert len(artifact.locators(must_exist=True)) == 2, \
        "artifact has been cached, so # of locators with must_exist should be 2"

    artifact.cache_delete()
    assert len(artifact.locators(must_exist=True)) == 1, \
        "artifact has been deleted from cache, so # of locators with must_exist should be 1"

def test_artifact_cache_put_mismatched_file_size(artifact):
    cached_url = artifact.cached_url()
    # write different content into cached_url so it has a different file size than the source.
    os.makedirs(os.path.dirname(cached_url))
    with open(cached_url, 'wt', encoding="utf-8") as file:
        file.write("DIFFERENT STUFF HERE")

    assert artifact.source.size(artifact) != artifact.caches[0].size(artifact), \
        "artifact source has different content than artifact cache, sizes should be different"

    artifact.cache_put()
    assert artifact.source.size(artifact) == artifact.caches[0].size(artifact), \
        "after cache_put, artifact source and cached location sizes should be the same."


def test_artifact_factory(
    fs_artifact_source,
    maven_artifact_source,
    fs_artifact_cache,
    fs_maven_artifact_cache
):
    available_sources = {
        'fs_source': fs_artifact_source,
        'maven_source': maven_artifact_source,
    }
    available_caches = {
        'fs_cache': fs_artifact_cache,
        'maven_cache': fs_maven_artifact_cache,
    }
    default_source_name = 'fs_source'
    default_cache_names = ['fs_cache']

    maven_artifact_config = {
        'id': 'org.apache.hadoop:hadoop-yarn-client:2.10.1',
        'source': 'maven_source',
        'caches': ['maven_cache']
    }
    maven_artifact = Artifact.factory(
        maven_artifact_config,
        available_sources,
        available_caches,
        default_source_name,
        default_cache_names,
        name='hadoop-yarn-client-2.10.1.jar',
    )
    assert maven_artifact.urls(must_exist=False) == [
        fs_maven_artifact_cache.url(maven_artifact),
        maven_artifact_source.url(maven_artifact),
    ], "Should instantiate an Artifact from config"

    # Since we provided default source and caches,
    # all this artifact needs is an id.
    fs_artifact_config = {
        'id': 'my_artifact.zip'
    }
    fs_artifact = Artifact.factory(
        fs_artifact_config,
        available_sources,
        available_caches,
        default_source_name,
        default_cache_names,
        # Don't strictly need a name, it will default to id.
    )
    assert fs_artifact.urls(must_exist=False) == [
        fs_artifact_cache.url(fs_artifact),
        fs_artifact_source.url(fs_artifact),
    ], "Should instantiate an Artifact from config with defaults"


def test_load_artifacts_from_config(
    artifact_configs,
    fs_artifact_source,
    maven_artifact_source,
    fs_artifact_cache,
    fs_maven_artifact_cache
):
    artifacts = Artifact.load_artifacts_from_config(artifact_configs)

    maven_artifact = artifacts['hadoop-yarn-client-2.10.1.jar']
    maven_artifact_id = artifact_configs['artifacts'][
        'hadoop-yarn-client-2.10.1.jar'
    ]['id']
    assert maven_artifact.id == maven_artifact_id
    assert maven_artifact.urls(must_exist=False) == [
        fs_maven_artifact_cache.url(maven_artifact),
        maven_artifact_source.url(maven_artifact),
    ]

    fs_artifact = artifacts['my_artifact.zip']
    fs_artifact_id = artifact_configs['artifacts']['my_artifact.zip']['id']
    assert fs_artifact.id == fs_artifact_id
    assert fs_artifact.urls(must_exist=False) == [
        fs_artifact_cache.url(fs_artifact),
        fs_artifact_source.url(fs_artifact),
    ]


def test_load_artifacts_from_config_with_incorrect_config(
    artifact_configs
):
    artifact_configs_for_test = deepcopy(artifact_configs)
    del artifact_configs_for_test['artifacts']['my_artifact.zip']['id']
    # ValueError, id is required
    with pytest.raises(ValueError):
        Artifact.load_artifacts_from_config(artifact_configs_for_test)

    artifact_configs_for_test = deepcopy(artifact_configs)
    del artifact_configs_for_test['default_artifact_source']
    # ValueError, source is required if no default is provided
    with pytest.raises(ValueError):
        Artifact.load_artifacts_from_config(artifact_configs_for_test)

    artifact_configs_for_test = deepcopy(artifact_configs)
    del artifact_configs_for_test['artifact_sources']['maven_source']
    # KeyError, since maven_source is not defined, but the
    # hadoop-yarn-client-2.10.1.jar artifact references it.
    with pytest.raises(KeyError):
        Artifact.load_artifacts_from_config(artifact_configs_for_test)

    artifact_configs_for_test = deepcopy(artifact_configs)
    del artifact_configs_for_test['artifact_sources']
    # ValueError, because now artifact_sources are not defined
    # at all, but it must be.
    with pytest.raises(ValueError):
        Artifact.load_artifacts_from_config(artifact_configs_for_test)
