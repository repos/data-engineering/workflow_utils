from __future__ import annotations
from typing import TYPE_CHECKING, Optional
from abc import ABC, abstractmethod
from workflow_utils.util import LogHelper


if TYPE_CHECKING:
    # We only need to import this for type hints.
    # If we do imports when not checking types, we get a circular import.
    # See: https://stackoverflow.com/a/39757388
    # Note: Importing annotations from __future__ allows us to
    # refer to the Artifact type directly, instead of as a String.
    from workflow_utils.artifact import Artifact


class ArtifactLocator(ABC, LogHelper):
    """
    An ArtifactLocator needs to be able to get an
    artifact's URL by artifact, as well as
    determine if the artifact exists at that URL.

    Both ArtifactSource and ArtifactCache are
    ArtifactLocators.
    """

    @abstractmethod
    def url(self, artifact: Artifact) -> str:
        """
        Gets the URL for the artifact.

        :param artifact: artifact, implementation specific.
        """

    @abstractmethod
    def exists(self, artifact: Artifact) -> bool:
        """
        :param artifact: artifact, implementation specific.
        """

    @abstractmethod
    def size(self, artifact: Artifact) -> Optional[int]:
        """
        Gets the file size of the artifact at artifact in bytes,
        or None if the file does not exist.

        :param artifact: artifact, implementation specific.
        """
