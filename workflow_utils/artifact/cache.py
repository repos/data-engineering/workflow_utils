from __future__ import annotations

from typing import TYPE_CHECKING, Optional, Callable

import os
import fsspec
from fsspec import AbstractFileSystem

from workflow_utils.util import safe_filename
from workflow_utils.artifact.maven import maven_artifact_uri
from workflow_utils.artifact.locator import ArtifactLocator

if TYPE_CHECKING:
    # We only need to import this for type hints.
    # If we do imports when not checking types, we get a circular import.
    # See: https://stackoverflow.com/a/39757388
    # Note: Importing annotations from __future__ allows us to
    # refer to the Artifact type directly, instead of as a String.
    from workflow_utils.artifact import Artifact


def cache_key_default(artifact: Artifact) -> str:
    """Default cache_key_fn implementation.
    Simply returns the artifact name with all its unsafe characters removed.

    :param artifact: Artifact
    """
    return safe_filename(artifact.name)

def cache_key_maven(artifact: Artifact) -> str:
    """Translates a coordinate to a relative maven path,
    allowing Maven artifacts to be cached in
    a Maven compatible directory hierarchy.

    :param artifact: Artifact. artifact.id should be a Maven coordinate.
    """
    return maven_artifact_uri(artifact.id)


class FsArtifactCache(ArtifactLocator):
    """
    ArtifactCache for use with any writable fsspec FileSystem.
    """

    def __init__(
        self,
        base_uri: str,
        cache_key_fn: Callable[[Artifact], str] = cache_key_default
    ):
        """Construct the FsArtifactCache object

        :param base_uri: Base URI in which to store cached artifacts. This URI should start with a supported fsspec protocol.
        :param cache_key_fn: A Callable that constructs the second part of the cache URI - accepts Artifact as argument.
        """
        self.base_uri: str = base_uri
        self.cache_key_fn = cache_key_fn
        super().__init__()
        # check the validity of base_uri by letting fsspec
        # create a file system based on it - fsspec will throw
        # exceptions if URI is invalid
        self.fs()

    def fs(self) -> AbstractFileSystem:  # pylint: disable=invalid-name
        """Returns the fsspec's FileSystem object based on this
        cache's base_uri.
        """
        source_fs, _ = fsspec.core.url_to_fs(self.base_uri)
        return source_fs

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}({self.base_uri})"

    def url(self, artifact: Artifact) -> str:
        """Returns a full URL to the given artifact in this cache.

        :param artifact: Artifact
        """
        return os.path.join(
            self.base_uri,
            self.cache_key_fn(artifact)
        )

    def exists(self, artifact: Artifact) -> bool:
        """Returns True if artifact exists in this cache.

        :param artifact: Artifact
        """
        return bool(self.fs().exists(self.url(artifact)))

    def size(self, artifact: Artifact) -> Optional[int]:
        """Retrieve size of artifact in this cache.

        :param artifact: Artifact
        """
        cache_fs = self.fs()
        artifact_url = self.url(artifact)

        if not cache_fs.exists(artifact_url) or cache_fs.isdir(artifact_url):
            return None

        return int(cache_fs.size(artifact_url))

    def put(self, artifact: Artifact) -> None:
        """Puts the artifact into its designated cache location,
        as determined by its self.url(artifact) method.

        :param artifact: Artifact
        """
        cache_fs = self.fs()

        # source.url ends with artifact.id
        url_source = cache_fs._strip_protocol(artifact.source.url(artifact))
        url_destination = self.url(artifact)

        try:
            if cache_fs.exists(url_destination):
                cache_fs.rm(url_destination, recursive=True)

            to_mkdir = cache_fs._parent(url_destination)
            cache_fs.mkdirs(to_mkdir, exist_ok=True)

            self.log.debug(f"About to cache put {url_source} into {url_destination}")
            cache_fs.put(url_source, url_destination, recursive=True)
        except Exception as e:  # pylint: disable=broad-exception-caught
            self.log.debug(f"Cache put Exception {e}")
            self.log.exception(e, stack_info=True)
            raise e

    def delete(self, artifact: Artifact) -> None:
        """Delete artifact from cache.

        :param artifact: Artifact
        """
        if self.exists(artifact):
            self.log.debug(f"Cache delete of {repr(artifact)}")
            self.fs().rm(self.url(artifact), recursive=True)


class FsMavenArtifactCache(FsArtifactCache):
    """DEPRECATED.
    This class will be removed in future versions. Leaving it here
    for backward compatibility with existing YAML configurations that
    reference this class.

    Caches Maven based artifacts using their coordinates
    in a Maven repository directory hierarchy in a writable file system.
    e.g. an artifact_id coordinate of
    'org.apache.hadoop:hadoop-yarn-client:2.10.1'
    would result in cache key of
    org/apache/hadoop/hadoop-yarn-client/2.10.1/hadoop-yarn-client-2.10.1.jar
    """

    def __init__(self, base_uri: str):
        super().__init__(base_uri, cache_key_fn=cache_key_maven)
